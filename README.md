Cette présentation a pour objectif d'expliquer les fondamentaux de la base de données du framework EPICS (Experimental Physics and Industrial Control System)<p>
The projet is public, you can download the presentation directly from GitLab with Download button.<br/>
From Linux, you can use the commands:
```
wget https://gitlab.com/formation-epics/anf-2022/1_3-notion-de-la-base-de-donnees-epics/-/archive/main/1_3-notion-de-la-base-de-donnees-epics-main.zip <br/>
unzip 1_3-notion-de-la-base-de-donnees-epics-main.zip
```
To modifiy the slides, you need to be a member of the project and clone the project.